# README #

The application uses Spring Boot and is setup to run as standalone app with an embedded servlet container (tomcat)

Clone the repository, then go to your command line, navigate to the parent directory (where the pom.xml exists) and run:

`mvn spring-boot:run`

The application can be accessed at:

`http://localhost:8080/`

The in memory db can be accessed at:

`http://localhost:8080/h2-console/`

leave the defaults change the JDBC URL to:

`JDBC URL jdbc:h2:mem:testdb`

Note: 

Jetty can be enabled by uncommenting marked sections in the pom.xml

The application can also be turned into  WAR (.war file) for deployment to other compliant application servers if desired