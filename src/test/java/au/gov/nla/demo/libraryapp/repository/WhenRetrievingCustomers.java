package au.gov.nla.demo.libraryapp.repository;

import au.gov.nla.demo.libraryapp.entity.Person;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;


@RunWith(SpringRunner.class)
@DataJpaTest
public class WhenRetrievingCustomers {

    @Autowired
    private PersonRepository personRepository;

    @Test
    public void should_return_all_customers() {
        List<Person> customers = personRepository.findAll();
        assertNotNull(customers);
        assertThat(customers.isEmpty(), is(false));
        assertThat(customers.size(), is(3));
    }

}