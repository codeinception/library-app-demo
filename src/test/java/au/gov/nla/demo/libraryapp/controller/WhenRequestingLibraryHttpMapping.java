package au.gov.nla.demo.libraryapp.controller;

import au.gov.nla.demo.libraryapp.entity.Book;
import au.gov.nla.demo.libraryapp.entity.Lent;
import au.gov.nla.demo.libraryapp.entity.Person;
import au.gov.nla.demo.libraryapp.service.LibraryService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(LibraryController.class)
public class WhenRequestingLibraryHttpMapping {

    public static final String TEXT_HTML_CHARSET_UTF_8 = "text/html;charset=UTF-8";

    @Autowired
    private MockMvc mvc;

    @MockBean
    private LibraryService libraryService;

    private List<Book> books;
    private List<Person> customers;
    private List<Lent> lents;

    @Before
    public void setUp() {
        setupDomain();
        given(libraryService.getBooks()).willReturn(books);
        given(libraryService.getCustomers()).willReturn(customers);
        given(libraryService.getBooksLentByPerson(1L)).willReturn(lents);

    }

    private void setupDomain() {
        //TODO refactor tests to remove duplicating domain as test cases increase
        Person person = new Person();
        person.setId(1L);
        person.setFullName("Customer 1");

        Book book = new Book ();
        book.setId(1L);
        book.setTitle("Book 1");

        Lent lent = new Lent();
        lent.setId(1L);
        lent.setPerson(person);
        lent.setBook(book);

        books = Arrays.asList(book);
        customers = Arrays.asList(person);
        lents = Arrays.asList(lent);
    }

    @After
    public void tearDown() {
        books = null;
        customers = null;
        lents = null;
    }


    @Test
    public void should_return_home_page() throws Exception {
        mvc.perform(get("/")
                .contentType(MediaType.ALL))
                .andExpect(content().contentType(TEXT_HTML_CHARSET_UTF_8))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("message", "books", "customers"))
                .andExpect(view().name(LibraryController.LANDING_PAGE_TEMPLATE)) ;
    }

    @Test
    public void should_return_lest_rest_api() throws Exception {
        mvc.perform(get(LibraryController.REST_API_PERSON_ID_LENT, 1L)
                .contentType(MediaType.ALL))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].book.id", is(1)))
                .andExpect(jsonPath("$[0].book.title", is("Book 1")))
                .andExpect(jsonPath("$[0].person.id", is(1)))
                .andExpect(jsonPath("$[0].person.fullName", is("Customer 1")));
    }

}