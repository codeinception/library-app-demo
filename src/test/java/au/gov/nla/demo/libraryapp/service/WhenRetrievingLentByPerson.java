package au.gov.nla.demo.libraryapp.service;

import au.gov.nla.demo.libraryapp.entity.Book;
import au.gov.nla.demo.libraryapp.entity.Lent;
import au.gov.nla.demo.libraryapp.entity.Person;
import au.gov.nla.demo.libraryapp.repository.BookRepository;
import au.gov.nla.demo.libraryapp.repository.LentRepository;
import au.gov.nla.demo.libraryapp.repository.PersonRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
public class WhenRetrievingLentByPerson {


    @TestConfiguration
    static class LibraryServiceTestContextConfiguration {

        @Bean
        public LibraryService libraryService() {
            return new LibraryService();
        }
    }

    @Autowired
    private LibraryService libraryService;

    @MockBean
    private PersonRepository personRepository;

    @MockBean
    private BookRepository bookRepository;

    @MockBean
    private LentRepository lentRepository;

    private List<Lent> lents;
    private Person person;
    private Book book;

    @Before
    public void setUp() {
        setupDomain();
        Mockito.when(personRepository.getOne(1L)).thenReturn(person);
        Mockito.when(lentRepository.getLentByPerson(person)).thenReturn(lents);
    }

    private void setupDomain() {
        //TODO refactor to builder/factor patterns when increasing test functionality
        person = new Person();
        person.setId(1L);

        book = new Book ();
        book.setId(1L);

        Lent lent = new Lent();
        lent.setId(1L);
        lent.setPerson(person);
        lent.setBook(book);

        lents = new ArrayList();
        lents.add(lent);
    }

    @After
    public void tearDown() {
        lents = null;
        person = null;
    }

    @Test
    public void should_return_all_lents() {

        List<Lent> result = libraryService.getBooksLentByPerson(1L);

        assertNotNull(result);
        assertThat(result.isEmpty(), is(false));
        assertThat(result.size(), is(1));
        assertEquals(result.get(0).getPerson(), person);
        assertEquals(result.get(0).getBook(), book);
    }



}