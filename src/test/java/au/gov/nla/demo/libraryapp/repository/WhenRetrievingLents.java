package au.gov.nla.demo.libraryapp.repository;

import au.gov.nla.demo.libraryapp.entity.Lent;
import au.gov.nla.demo.libraryapp.entity.Person;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;


@RunWith(SpringRunner.class)
@DataJpaTest
public class WhenRetrievingLents {

    @Autowired
    private LentRepository lentRepository;

    @Autowired
    private PersonRepository personRepository;

    @Test
    public void should_return_all_lents() {
        List<Lent> lents = lentRepository.findAll();
        assertNotNull(lents);
        assertThat(lents.isEmpty(), is(false));
        assertThat(lents.size(), is(3));
    }

    @Test
    public void should_return_all_lents_by_customer() {
        Person person = personRepository.getOne(1L);
        List<Lent> lents = lentRepository.getLentByPerson(person);
        assertNotNull(lents);
        assertThat(lents.isEmpty(), is(false));
        assertThat(lents.size(), is(2));
    }

}