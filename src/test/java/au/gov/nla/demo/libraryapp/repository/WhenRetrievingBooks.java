package au.gov.nla.demo.libraryapp.repository;

import au.gov.nla.demo.libraryapp.entity.Book;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;


@RunWith(SpringRunner.class)
@DataJpaTest
public class WhenRetrievingBooks {

    @Autowired
    private BookRepository bookRepository;

    @Test
    public void should_return_all_books() {
        List<Book> books = bookRepository.findAll();
        assertNotNull(books);
        assertThat(books.isEmpty(), is(false));
        assertThat(books.size(), is(6));
    }

}