package au.gov.nla.demo.libraryapp.repository;


import au.gov.nla.demo.libraryapp.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookRepository extends JpaRepository<Book, Long> {

}
