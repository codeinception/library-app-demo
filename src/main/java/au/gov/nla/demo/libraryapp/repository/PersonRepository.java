package au.gov.nla.demo.libraryapp.repository;


import au.gov.nla.demo.libraryapp.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepository extends JpaRepository<Person, Long> {
}
