package au.gov.nla.demo.libraryapp.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;


@Entity
@NamedQuery(name = Lent.Queries.findLentByPerson,
        query = "SELECT lt FROM Lent lt "
                + "WHERE lt.person = :" + Lent.Queries.Parameters.PERSON)
public class Lent {

    /*
      Convenience to expose type safety and reduce errors for named queries
    */
    public interface Queries {

        String findLentByPerson = "Lent.findLentByPerson";

        interface Parameters {

            String PERSON = "person";
        }
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @JsonIgnoreProperties({"lent"})
    @OneToOne(optional = false)
    @JoinColumn(name = "book_id")
    private Book book;

    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToOne(optional = false)
    @JoinColumn(name = "person_id")
    private Person person;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}
