package au.gov.nla.demo.libraryapp.repository;


import au.gov.nla.demo.libraryapp.entity.Lent;
import au.gov.nla.demo.libraryapp.entity.Person;

import java.util.List;

public interface LentRepositoryCustom {
    List<Lent> getLentByPerson(Person person);
}
