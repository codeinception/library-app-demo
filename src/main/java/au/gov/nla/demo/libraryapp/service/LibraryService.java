package au.gov.nla.demo.libraryapp.service;

import au.gov.nla.demo.libraryapp.entity.Book;
import au.gov.nla.demo.libraryapp.entity.Lent;
import au.gov.nla.demo.libraryapp.entity.Person;
import au.gov.nla.demo.libraryapp.repository.BookRepository;
import au.gov.nla.demo.libraryapp.repository.LentRepository;
import au.gov.nla.demo.libraryapp.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

/* TODO This service is broad, look to refactor to a finer granularity if required */

@Service
public class LibraryService {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private LentRepository lentRepository;

    public List<Book> getBooks () {
        return bookRepository.findAll();
    }

    public List<Person> getCustomers () {
        return personRepository.findAll();
    }

    public List<Lent> getBooksLentByPerson (Long personId) {
        Person person = personRepository.getOne(personId);
        if (person != null) {
            return lentRepository.getLentByPerson(person);
        }
        return Collections.emptyList();
    }

}
