package au.gov.nla.demo.libraryapp.repository;

import au.gov.nla.demo.libraryapp.entity.Lent;
import au.gov.nla.demo.libraryapp.entity.Person;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Collections;
import java.util.List;

public class LentRepositoryCustomImpl implements LentRepositoryCustom {

    private final static Log logger = LogFactory.getLog(LentRepositoryCustomImpl.class);

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Lent> getLentByPerson(Person person) {
        if (person == null) {
            logger.warn("Invalid person passed to getLentByPerson method");
            return Collections.EMPTY_LIST;
        }
        Query query = em.createNamedQuery(Lent.Queries.findLentByPerson);
        query.setParameter(Lent.Queries.Parameters.PERSON, person);
        return query.getResultList();
    }
}
