package au.gov.nla.demo.libraryapp.repository;


import au.gov.nla.demo.libraryapp.entity.Lent;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LentRepository extends JpaRepository<Lent, Long>, LentRepositoryCustom {

}
