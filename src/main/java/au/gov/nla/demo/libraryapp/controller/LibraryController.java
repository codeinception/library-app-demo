package au.gov.nla.demo.libraryapp.controller;

import au.gov.nla.demo.libraryapp.entity.Lent;
import au.gov.nla.demo.libraryapp.service.LibraryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class LibraryController {

    public static final String LANDING_PAGE_TEMPLATE = "home";
    public static final String REST_API_PERSON_ID_LENT = "/api/person/{id}/lent";

    @Autowired
    LibraryService libraryService;

    // inject via application.properties
    @Value("${welcome.message:Hello World!}")
    private String message = "Hello World";

    @GetMapping("/")
    String home(ModelMap model) {
        model.addAttribute("message", message);
        model.addAttribute("books", libraryService.getBooks());
        model.addAttribute("customers", libraryService.getCustomers());
        return LANDING_PAGE_TEMPLATE;
    }


    /* TODO: Refactor to RestController when more rest methods are added */

    @GetMapping(REST_API_PERSON_ID_LENT)
    @ResponseBody
    public List<Lent> lent(@PathVariable Long id) {
        return libraryService.getBooksLentByPerson(id);
    }
    
}