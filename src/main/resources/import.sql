-- Person data
insert into PERSON (ID, FULL_NAME, PHONE, EMAIL, LENDING) values (1L, 'Customer 1', '0400000001', 'email1@email.com', true);
insert into PERSON (ID, FULL_NAME, PHONE, EMAIL, LENDING) values (2L, 'Customer 2', '0400000002', 'email2@email.com', true);
insert into PERSON (ID, FULL_NAME, PHONE, EMAIL, LENDING) values (3L, 'Customer 3', '0400000003', 'email3@email.com', false);

-- Book data
insert into BOOK (ID, TITLE, ISBN, AUTHOR, AVAILABLE) values (1L, 'Library Book 1', '000000000001', 'Test Author 1', false);
insert into BOOK (ID, TITLE, ISBN, AUTHOR, AVAILABLE) values (2L, 'Library Book 2', '000000000002', 'Test Author 2', false);
insert into BOOK (ID, TITLE, ISBN, AUTHOR, AVAILABLE) values (3L, 'Library Book 3', '000000000003', 'Test Author 3', true);
insert into BOOK (ID, TITLE, ISBN, AUTHOR, AVAILABLE) values (4L, 'Library Book 4', '000000000004', 'Test Author 4', false);
insert into BOOK (ID, TITLE, ISBN, AUTHOR, AVAILABLE) values (5L, 'Library Book 5', '000000000005', 'Test Author 5', true);
insert into BOOK (ID, TITLE, ISBN, AUTHOR, AVAILABLE) values (6L, 'Library Book 6', '000000000006', 'Test Author 6', true);

-- Lent data
insert into LENT (ID, BOOK_ID, PERSON_ID) values (1L, 1L, 1L);
insert into LENT (ID, BOOK_ID, PERSON_ID) values (2L, 2L, 1L);
insert into LENT (ID, BOOK_ID, PERSON_ID) values (3L, 4L, 2L);
