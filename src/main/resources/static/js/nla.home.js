$(function () {
    $('[name=\"btnBookDetail\"]').click(getBookDetail);
    $('#btnCloseDetail').click(closeBookDetail);
});

function closeBookDetail() {
    $('#collapseExample').collapse('hide');
}

function getBookDetail() {
    var personId = $(this).attr('id');
    var lentUrl = '/api/person/' + personId + '/lent';

    $.getJSON(lentUrl, function( data ) {

        var bookDetailsHtml = '<p>Book lent details for <strong>' + data[0].person.fullName + '</strong></p>';

        $.each( data, function( key, val ) {
            bookDetailsHtml +='<p><mark>Id: </mark><strong>' + val.book.id + '</strong>';
            bookDetailsHtml +=' <mark>Title:</mark><strong> ' + val.book.title + '</strong>';
            bookDetailsHtml +=' <mark>Author:</mark><strong> ' + val.book.author + '</strong>';
            bookDetailsHtml +=' <mark>Isbn:</mark><strong> ' + val.book.isbn + '</strong>';
            bookDetailsHtml += '</p>';
        });

        $('#bookDetailText').html(bookDetailsHtml);
        $('#collapseExample').collapse('show');
    });
}




